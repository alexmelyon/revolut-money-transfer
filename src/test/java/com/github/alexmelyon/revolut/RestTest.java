package com.github.alexmelyon.revolut;

import com.github.alexmelyon.revolut.account.AccountService;
import com.github.alexmelyon.revolut.account.SimpleAccountService;
import com.github.alexmelyon.revolut.checkers.*;
import com.github.alexmelyon.revolut.transfer.ManyThreadsTransferService;
import com.github.alexmelyon.revolut.transfer.TransferService;
import com.google.gson.JsonParser;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RestTest extends AbstractModule {

    RevolutService main;
    TransferService transferService;

    @BeforeClass
    public void setUp() {
        Injector injector = Guice.createInjector(this);
        main = injector.getInstance(RevolutService.class);
        transferService = injector.getInstance(TransferService.class);
        main.startServer(injector);
    }

    @AfterClass
    public void tearDown() {
        main.stopServer();
    }

    @Override
    protected void configure() {
        bind(AccountService.class).to(SimpleAccountService.class);
        bind(TransferService.class).to(ManyThreadsTransferService.class);
        bind(RevolutService.class).to(Main.class);
        Multibinder<TransferChecker> checkerBinder = Multibinder.newSetBinder(binder(), TransferChecker.class);
        checkerBinder.addBinding().to(NegativeBalanceChecker.class);
        checkerBinder.addBinding().to(NegativeDeltaChecker.class);
        checkerBinder.addBinding().to(SameAccountChecker.class);
        checkerBinder.addBinding().to(NonexistentAccountChecker.class);
    }

    @Test
    public void testRoutes() throws IOException, InterruptedException {
        JsonParser parser = new JsonParser();
        String response = request("GET", "/");
        Assert.assertEquals(response, "ROOT");
        response = request("POST", "/account/new");
        Assert.assertEquals(parser.parse(response).getAsJsonObject().get("accountId").getAsInt(), 0);
        response = request("POST", "/account/new");
        Assert.assertEquals(parser.parse(response).getAsJsonObject().get("accountId").getAsInt(), 1);
        transferService.waitForTransfers(3);
        response = request("POST", "/account/0/deposit/10");
        Assert.assertEquals(parser.parse(response).getAsJsonObject().get("delta").getAsInt(), 10);
        response = request("POST", "/account/0/withdraw/5");
        Assert.assertEquals(parser.parse(response).getAsJsonObject().get("delta").getAsInt(), 5);
        response = request("POST", "/transfer/from/0/to/1/delta/5");
        Assert.assertEquals(parser.parse(response).getAsJsonObject().get("delta").getAsInt(), 5);
        transferService.await();
        response = request("GET", "/account/0");
        Assert.assertEquals(parser.parse(response).getAsJsonObject().get("balance").getAsLong(), 0);
        response = request("GET", "/account/1");
        Assert.assertEquals(parser.parse(response).getAsJsonObject().get("balance").getAsLong(), 5);
    }

    private String request(String method, String path) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL("http://localhost:4567" + path).openConnection();
        connection.setRequestMethod(method);
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        return reader.readLine();
    }
}
