package com.github.alexmelyon.revolut.transfer;

import com.github.alexmelyon.revolut.Main;
import com.github.alexmelyon.revolut.RevolutService;
import com.github.alexmelyon.revolut.account.Account;
import com.github.alexmelyon.revolut.account.AccountService;
import com.github.alexmelyon.revolut.account.SimpleAccountService;
import com.github.alexmelyon.revolut.checkers.*;
import com.github.alexmelyon.revolut.exceptions.NonexistentAccountException;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.multibindings.Multibinder;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.ExecutionException;

public class SingleThreadTransferServiceTest extends AbstractModule {

    AccountService accountService;
    TransferService transferService;
    RevolutService main;

    @Override
    protected void configure() {
        bind(AccountService.class).to(SimpleAccountService.class);
        bind(TransferService.class).to(SingleThreadTransferService.class);
        bind(RevolutService.class).to(Main.class);
        Multibinder<TransferChecker> checkerBinder = Multibinder.newSetBinder(binder(), TransferChecker.class);
        checkerBinder.addBinding().to(NegativeBalanceChecker.class);
        checkerBinder.addBinding().to(NegativeDeltaChecker.class);
        checkerBinder.addBinding().to(SameAccountChecker.class);
        checkerBinder.addBinding().to(NonexistentAccountChecker.class);
    }

    @BeforeMethod
    public void setUp() {
        Injector injector = Guice.createInjector(this);
        main = injector.getInstance(RevolutService.class);
        accountService = injector.getInstance(AccountService.class);
        transferService = injector.getInstance(TransferService.class);
    }

    @Test
    public void shouldCreateAccount() {
        main.accountCreate();
        Assert.assertEquals(accountService.getAccountsCount(), 1);
    }

    @Test
    public void shouldGetAccount() {
        main.accountCreate();
        Object account = main.accountGet(0);
        Assert.assertTrue(account instanceof Account);
    }

    @Test(expectedExceptions = NonexistentAccountException.class)
    public void shouldGetErrorNonExisting() {
        Object account = main.accountGet(0);
    }

    @Test(invocationCount = 1000)
    public void shouldDeposit() throws InterruptedException, ExecutionException {
        main.accountCreate();
        transferService.waitForTransfers(1);
        main.accountDeposit(0, 10, false);
        transferService.await();
        Account account = (Account) main.accountGet(0);
        long actual = account.getBalance();
        Assert.assertEquals(actual, 10L);
    }

    @Test(invocationCount = 100)
    public void shouldDepositSync() throws InterruptedException, ExecutionException {
        main.accountCreate();
        main.accountDeposit(0, 10, true);
        Account account = (Account) main.accountGet(0);
        long actual = account.getBalance();
        Assert.assertEquals(actual, 10L);
    }

    @Test(invocationCount = 1000)
    public void shouldWithdraw() throws InterruptedException, ExecutionException {
        main.accountCreate();
        transferService.waitForTransfers(2);
        main.accountDeposit(0, 10, false);
        main.accountWithdraw(0, 5, false);
        Account account = (Account) main.accountGet(0);
        transferService.await();
        long actual = account.getBalance();
        Assert.assertEquals(actual, 5L);
    }

    @Test(invocationCount = 100)
    public void shouldWithdrawSync() throws InterruptedException, ExecutionException {
        main.accountCreate();
        main.accountDeposit(0, 10, true);
        main.accountWithdraw(0, 5, true);
        Account account = (Account) main.accountGet(0);
        long actual = account.getBalance();
        Assert.assertEquals(actual, 5L);
    }

    @Test(invocationCount = 1000)
    public void shouldTransfer() throws InterruptedException, ExecutionException {
        main.accountCreate();
        main.accountCreate();
        transferService.waitForTransfers(2);
        main.accountDeposit(0, 10, false);
        main.transfer(0, 1, 10L, false);
        Account account = (Account) main.accountGet(1);
        transferService.await();
        long actual = account.getBalance();
        Assert.assertEquals(actual, 10L);
    }

    @Test(invocationCount = 100)
    public void shouldTransferSync() throws ExecutionException, InterruptedException {
        main.accountCreate();
        main.accountCreate();
        main.accountDeposit(0, 10, true);
        main.transfer(0, 1, 10L, true);
        Account account = (Account) main.accountGet(1);
        long actual = account.getBalance();
        Assert.assertEquals(actual, 10L);
    }
}