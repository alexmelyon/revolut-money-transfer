package com.github.alexmelyon.revolut;

import com.google.inject.Injector;

import java.rmi.NoSuchObjectException;
import java.util.concurrent.ExecutionException;

public interface RevolutService {

    void startServer(Injector injector);

    void stopServer();

    Object accountCreate();

    Object accountGet(int accountId);

    Object accountWithdraw(int accountId, long delta, boolean sync) throws ExecutionException, InterruptedException;

    Object accountDeposit(int accountId, long delta, boolean sync) throws ExecutionException, InterruptedException;

    Object transfer(int src, int dst, long delta, boolean sync) throws ExecutionException, InterruptedException;
}
