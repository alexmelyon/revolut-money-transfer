package com.github.alexmelyon.revolut.account;

import com.google.gson.JsonObject;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {

    private Integer accountId = 0;
    long balance = 0;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Long getBalance() {
        return balance;
    }

    public void changeBalance(Long delta) {
        balance += delta;
    }

    public String toString() {
        JsonObject json = new JsonObject();
        json.addProperty("accountId", getAccountId());
        json.addProperty("balance", getBalance());
        return json.toString();
    }
}
