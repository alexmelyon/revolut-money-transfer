package com.github.alexmelyon.revolut.account;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Singleton
public class SimpleAccountService implements AccountService {

    List<Account> accounts = Collections.synchronizedList(new ArrayList<>());

    @Override
    public Account createAccount() {
        Account account = new Account();
        account.setAccountId(accounts.size());
        accounts.add(account);
        return account;
    }

    @Override
    public Account getAccount(Integer accountId) {
        return accounts.get(accountId);
    }

    @Override
    public int getAccountsCount() {
        return accounts.size();
    }

    @Override
    public boolean isAccountExists(Integer accountId) {
        return accountId < accounts.size();
    }
}
