package com.github.alexmelyon.revolut.account;

public interface AccountService {

    Account createAccount();

    Account getAccount(Integer accountId);

    boolean isAccountExists(Integer accountId);

    int getAccountsCount();
}
