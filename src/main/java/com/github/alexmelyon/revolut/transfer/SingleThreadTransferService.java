package com.github.alexmelyon.revolut.transfer;

import com.github.alexmelyon.revolut.account.Account;
import com.github.alexmelyon.revolut.account.AccountService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Singleton
public class SingleThreadTransferService extends AbstractTransferService {

    @Inject
    AccountService accountService;

    ExecutorService executor = Executors.newSingleThreadExecutor();

    @Override
    public Future<Transfer> processTransfer(Transfer transfer) {
        Future<Transfer> future = executor.submit(new Callable<Transfer>() {
            @Override
            public Transfer call() throws Exception {
                try {
                    if (!checkers.stream().allMatch(it -> it.throwIfTransferNotOk(transfer))) {
                        transfer.setSuccess(false);
                        return transfer;
                    }
                    if (transfer.getSrc() != null) {
                        Account src = accountService.getAccount(transfer.getSrc());
                        src.changeBalance(-transfer.getDelta());
                    }
                    if (transfer.getDst() != null) {
                        Account dst = accountService.getAccount(transfer.getDst());
                        dst.changeBalance(transfer.getDelta());
                    }
                    transfer.setSuccess(true);

                    if (countDownLatch != null) {
                        countDownLatch.countDown();
                    }
                    return transfer;
                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        });
        return future;
    }
}
