package com.github.alexmelyon.revolut.transfer;

import java.util.concurrent.Future;

public interface TransferService {

    Transfer createTransfer();

    Future<Transfer> submit(Transfer transfer);

    void waitForTransfers(int count);

    void await() throws InterruptedException;

}
