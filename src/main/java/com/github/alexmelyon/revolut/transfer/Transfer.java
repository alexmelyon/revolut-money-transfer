package com.github.alexmelyon.revolut.transfer;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.concurrent.atomic.AtomicLong;

public class Transfer {

    private Long id;
    private Integer src;
    private Integer dst;
    private Long delta;
    private Boolean isSuccess;

    Transfer() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSrc() {
        return src;
    }

    public void setSrc(Integer src) {
        this.src = src;
    }

    public Integer getDst() {
        return dst;
    }

    public void setDst(Integer dst) {
        this.dst = dst;
    }

    public Long getDelta() {
        return delta;
    }

    public void setDelta(Long delta) {
        this.delta = delta;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

    public String toString() {
        JsonObject json = new JsonObject();
        json.addProperty("id", getId());
        json.addProperty("src", getSrc());
        json.addProperty("dst", getDst());
        json.addProperty("delta", getDelta());
        json.addProperty("success", isSuccess);
        return json.toString();
    }
}
