package com.github.alexmelyon.revolut.transfer;

import com.github.alexmelyon.revolut.account.Account;
import com.github.alexmelyon.revolut.account.AccountService;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Singleton
public class ManyThreadsTransferService extends AbstractTransferService {

    @Inject
    AccountService accountService;

    private ExecutorService executor = Executors.newFixedThreadPool(getProcCount());
    private Map<DoubleAccount, Lock> lockers = Collections.synchronizedMap(new HashMap<>());


    private static int getProcCount() {
        return Runtime.getRuntime().availableProcessors();
    }

    @Override
    public Future<Transfer> processTransfer(Transfer incoming) {
        Future<Transfer> future = executor.submit(new TransferProcessor(incoming, this));
        return future;
    }

    static class TransferProcessor implements Callable<Transfer> {

        Transfer transfer;
        ManyThreadsTransferService transferService;

        public TransferProcessor(Transfer transfer, TransferService transferService) {
            this.transfer = transfer;
            this.transferService = (ManyThreadsTransferService) transferService;
        }

        @Override
        public Transfer call() throws Exception {
            DoubleAccount doubleAccount = null;
            Lock locker = null;
            try {
                doubleAccount = new DoubleAccount(transfer.getSrc(), transfer.getDst());
                if (!transferService.lockers.containsKey(doubleAccount)) {
                    transferService.lockers.put(doubleAccount, new ReentrantLock());
                }
                locker = transferService.lockers.get(doubleAccount);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }

            while (true) {
                try {
                    locker.lock();
                    if (transferService.checkers.stream().allMatch(it -> it.throwIfTransferNotOk(transfer))) {
                        // Retry
                    }
                    if (transfer.getSrc() != null) {
                        Account src = transferService.accountService.getAccount(transfer.getSrc());
                        src.changeBalance(-transfer.getDelta());
                    }
                    if (transfer.getDst() != null) {
                        Account dst = transferService.accountService.getAccount(transfer.getDst());
                        dst.changeBalance(transfer.getDelta());
                    }
                    transfer.setSuccess(true);

                    if (transferService.countDownLatch != null) {
                        transferService.countDownLatch.countDown();
                    }
                    break;
                } catch (Exception e) {
                    // Retry
                } finally {
                    locker.unlock();
                    transferService.lockers.remove(doubleAccount);
                }
            }
            return transfer;
        }
    }

    static class DoubleAccount {
        private Integer less;
        private Integer more;

        DoubleAccount(Integer first, Integer second) {
            if (first == null || second == null) {
                less = first;
                more = second;
            } else {
                if (first < second) {
                    less = first;
                    more = second;
                } else {
                    less = second;
                    more = first;
                }
            }
        }

        @Override
        public int hashCode() {
            if (less == null) {
                return more;
            } else if (more == null) {
                return less;
            }
            return less ^ more;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DoubleAccount)) {
                return false;
            }
            DoubleAccount other = (DoubleAccount) obj;
            if (less == null) {
                return this.more.equals(other.more);
            } else if (more == null) {
                return this.less.equals(other.less);
            }
            return this.less.equals(other.less) && this.more.equals(other.more);
        }
    }
}
