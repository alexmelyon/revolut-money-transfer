package com.github.alexmelyon.revolut.transfer;

import com.github.alexmelyon.revolut.checkers.TransferChecker;
import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public abstract class AbstractTransferService implements TransferService {

    @Inject
    Set<TransferChecker> checkers;

    protected List<Transfer> transfers = Collections.synchronizedList(new ArrayList<>());
    protected volatile CountDownLatch countDownLatch;
    public volatile AtomicLong nextId = new AtomicLong(0L);

    @Override
    public Transfer createTransfer() {
        Transfer transfer = new Transfer();
        transfer.setId(nextId.getAndIncrement());
        return transfer;
    }

    @Override
    public Future<Transfer> submit(Transfer transfer) {
        transfers.add(transfer);
        return processTransfer(transfer);
    }

    protected abstract Future<Transfer> processTransfer(Transfer transfer);

    @Override
    public void waitForTransfers(int count) {
        countDownLatch = new CountDownLatch(count);
    }

    @Override
    public void await() throws InterruptedException {
        countDownLatch.await();
        countDownLatch = null;
    }
}
