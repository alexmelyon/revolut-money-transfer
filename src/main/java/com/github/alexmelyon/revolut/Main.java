package com.github.alexmelyon.revolut;

import com.github.alexmelyon.revolut.account.Account;
import com.github.alexmelyon.revolut.account.AccountService;
import com.github.alexmelyon.revolut.account.SimpleAccountService;
import com.github.alexmelyon.revolut.checkers.*;
import com.github.alexmelyon.revolut.exceptions.NonexistentAccountException;
import com.github.alexmelyon.revolut.exceptions.RevolutError;
import com.github.alexmelyon.revolut.exceptions.RevolutException;
import com.github.alexmelyon.revolut.transfer.ManyThreadsTransferService;
import com.github.alexmelyon.revolut.transfer.Transfer;
import com.github.alexmelyon.revolut.transfer.TransferService;
import com.google.gson.JsonObject;
import com.google.inject.*;
import com.google.inject.multibindings.Multibinder;
import spark.ExceptionHandler;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Singleton
public class Main extends AbstractModule implements RevolutService {

    @Inject
    AccountService accountService;
    @Inject
    TransferService transferService;

    public static void main(String[] args) {
        Main main = new Main();
        main.startServer(Guice.createInjector(main));
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Shouting down ...");
                main.stopServer();
                System.out.println("Server stopped");
            }
        }));
    }

    @Override
    public void startServer(Injector injector) {
        RevolutService server = injector.getInstance(RevolutService.class);
        Spark.exception(RevolutException.class, new ExceptionHandler<RevolutException>() {
            @Override
            public void handle(RevolutException e, Request request, Response response) {
                e.printStackTrace();
                response.status(400);
                JsonObject json = new JsonObject();
                json.addProperty("error", e.errorCode.code);
                json.addProperty("cause", e.message);
                response.body(json.toString());
            }
        });
        Spark.before((req, res) -> {
            String path = req.pathInfo();
            if (!path.equals("/") && path.endsWith("/")) {
                res.redirect(path.substring(0, path.length() - 1));
            }
        });

        Spark.get("/", (req, res) -> "ROOT");
        Spark.post("/account/new", (req, res) -> server.accountCreate());
        Spark.get("/account/:accountId", (req, res) -> {
            int accountId = parseInt(req.params(":accountId"), "WrongField 'accountId'");
            return server.accountGet(accountId);
        });

        Spark.post("/account/:accountId/withdraw/:delta", (req, res) -> {
            int accountId = parseInt(req.params(":accountId"), "Wrong field 'accountId'");
            long delta = parseLong(req.params(":delta"), "Wrong field 'delta'");
            boolean sync = Boolean.parseBoolean(req.queryParams("sync"));
            return server.accountWithdraw(accountId, delta, sync);
        });
        Spark.post("/account/:accountId/deposit/:delta", (req, res) -> {
            int accountId = parseInt(req.params(":accountId"), "Wrong field 'accountId'");
            long delta = parseLong(req.params(":delta"), "Wrong field 'delta'");
            boolean sync = Boolean.parseBoolean(req.queryParams("sync"));
            return server.accountDeposit(accountId, delta, sync);
        });

        Spark.post("/transfer/from/:src/to/:dst/delta/:delta", (req, res) -> {
            int src = parseInt(req.params(":src"), "Wrong field 'src'");
            int dst = parseInt(req.params(":dst"), "Wrong field 'dst'");
            long delta = parseLong(req.params(":delta"), "Wrong field 'delta'");
            boolean sync = Boolean.valueOf(req.queryParams("sync"));
            return server.transfer(src, dst, delta, sync);
        });

        Spark.awaitInitialization();
        System.out.println("Started http://localhost:4567/");
    }

    @Override
    public void stopServer() {
        Spark.stop();
        Spark.awaitStop();
    }

    @Override
    protected void configure() {
        bind(AccountService.class).to(SimpleAccountService.class);
        bind(TransferService.class).to(ManyThreadsTransferService.class);
        bind(RevolutService.class).to(Main.class);
        Multibinder<TransferChecker> checkerBinder = Multibinder.newSetBinder(binder(), TransferChecker.class);
        checkerBinder.addBinding().to(NegativeBalanceChecker.class);
        checkerBinder.addBinding().to(NegativeDeltaChecker.class);
        checkerBinder.addBinding().to(SameAccountChecker.class);
        checkerBinder.addBinding().to(NonexistentAccountChecker.class);
    }

    public Object accountGet(int accountId) {
        if (!accountService.isAccountExists(accountId)) {
            throw new NonexistentAccountException(accountId);
        }
        return accountService.getAccount(accountId);
    }

    public Object accountCreate() {
        Account account = accountService.createAccount();
        return account;
    }

    public Object accountWithdraw(int accountId, long delta, boolean sync) throws ExecutionException, InterruptedException {
        Transfer transfer = transferService.createTransfer();
        transfer.setSrc(accountId);
        transfer.setDelta(delta);
        Future<Transfer> future = transferService.submit(transfer);
        if (sync) {
            return future.get();
        } else {
            return transfer;
        }
    }

    public Object accountDeposit(int accountId, long delta, boolean sync) throws ExecutionException, InterruptedException {
        Transfer transfer = transferService.createTransfer();
        transfer.setDst(accountId);
        transfer.setDelta(delta);
        Future<Transfer> future = transferService.submit(transfer);
        if (sync) {
            return future.get();
        } else {
            return transfer;
        }
    }

    public Object transfer(int src, int dst, long delta, boolean sync) throws ExecutionException, InterruptedException {
        Transfer transfer = transferService.createTransfer();
        transfer.setSrc(src);
        transfer.setDst(dst);
        transfer.setDelta(delta);
        Future<Transfer> future = transferService.submit(transfer);
        if (sync) {
            return future.get();
        } else {
            return transfer;
        }
    }

    private static int parseInt(String from, String wrongMessage) {
        try {
            return Integer.parseInt(from);
        } catch (NumberFormatException e) {
            throw new RevolutException(RevolutError.WRONG_FIELD, wrongMessage);
        }
    }

    private static long parseLong(String from, String wrongMessage) {
        try {
            return Long.valueOf(from);
        } catch (NumberFormatException e) {
            throw new RevolutException(RevolutError.WRONG_FIELD, wrongMessage);
        }
    }
}
