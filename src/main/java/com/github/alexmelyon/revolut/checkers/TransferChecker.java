package com.github.alexmelyon.revolut.checkers;

import com.github.alexmelyon.revolut.transfer.Transfer;

public interface TransferChecker {

    boolean throwIfTransferNotOk(Transfer transfer);

}
