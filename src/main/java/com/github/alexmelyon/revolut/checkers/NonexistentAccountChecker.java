package com.github.alexmelyon.revolut.checkers;

import com.github.alexmelyon.revolut.account.AccountService;
import com.github.alexmelyon.revolut.exceptions.NonexistentAccountException;
import com.github.alexmelyon.revolut.exceptions.RevolutError;
import com.github.alexmelyon.revolut.exceptions.RevolutException;
import com.github.alexmelyon.revolut.transfer.Transfer;
import com.google.inject.Inject;

public class NonexistentAccountChecker implements TransferChecker {

    @Inject
    AccountService accountService;

    @Override
    public boolean throwIfTransferNotOk(Transfer transfer) {
        if (transfer.getSrc() != null) {
            if (!accountService.isAccountExists(transfer.getSrc())) {
                throw new NonexistentAccountException(transfer.getSrc());
            }
        }
        if(transfer.getDst() != null) {
            if(!accountService.isAccountExists(transfer.getDst())) {
                throw new NonexistentAccountException(transfer.getDst());
            }
        }
        return true;
    }
}
