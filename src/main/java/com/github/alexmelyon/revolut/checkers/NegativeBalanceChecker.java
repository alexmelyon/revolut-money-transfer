package com.github.alexmelyon.revolut.checkers;

import com.github.alexmelyon.revolut.account.AccountService;
import com.github.alexmelyon.revolut.exceptions.RevolutError;
import com.github.alexmelyon.revolut.exceptions.RevolutException;
import com.github.alexmelyon.revolut.transfer.Transfer;
import com.google.inject.Inject;

public class NegativeBalanceChecker implements TransferChecker {

    @Inject
    AccountService accountService;

    @Override
    public boolean throwIfTransferNotOk(Transfer transfer) {
        if (transfer.getSrc() != null) {
            if (accountService.getAccount(transfer.getSrc()).getBalance() >= transfer.getDelta()) {
                return true;
            }
            throw new RevolutException(RevolutError.NEGATIVE_BALANCE, String.format("Negative balance '%d'", transfer.getDelta()));
        }
        return true;
    }
}
