package com.github.alexmelyon.revolut.checkers;

import com.github.alexmelyon.revolut.exceptions.RevolutError;
import com.github.alexmelyon.revolut.exceptions.RevolutException;
import com.github.alexmelyon.revolut.transfer.Transfer;

public class NegativeDeltaChecker implements TransferChecker {
    @Override
    public boolean throwIfTransferNotOk(Transfer transfer) {
        if(transfer.getDelta() > 0) {
            return true;
        }
        throw new RevolutException(RevolutError.NEGATIVE_DELTA, String.format("Negative delta '%d'", transfer.getDelta()));
    }
}
