package com.github.alexmelyon.revolut.checkers;

import com.github.alexmelyon.revolut.exceptions.RevolutError;
import com.github.alexmelyon.revolut.exceptions.RevolutException;
import com.github.alexmelyon.revolut.transfer.Transfer;

public class SameAccountChecker implements TransferChecker {
    @Override
    public boolean throwIfTransferNotOk(Transfer transfer) {
        if (transfer.getSrc() != null && transfer.getDst() != null) {
            if(!transfer.getSrc().equals(transfer.getDst())) {
                return true;
            }
            throw new RevolutException(RevolutError.ACCOUNTS_IDENTICAL, String.format("Account IDs are identical '%d'", transfer.getSrc()));
        }
        return true;
    }
}
