package com.github.alexmelyon.revolut.exceptions;

public class RevolutException extends RuntimeException {

    public RevolutError errorCode;
    public String message;

    public RevolutException(RevolutError code, String message) {
        this.errorCode = code;
        this.message = message;
    }
}
