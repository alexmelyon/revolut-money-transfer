package com.github.alexmelyon.revolut.exceptions;

public enum RevolutError {
    WRONG_FIELD(0),
    NONEXISTENT_ACCOUNT(1),
    NEGATIVE_DELTA(2),
    NEGATIVE_BALANCE(3),
    ACCOUNTS_IDENTICAL(4);

    public int code;

    RevolutError(int code) {
        this.code = code;
    }
}
