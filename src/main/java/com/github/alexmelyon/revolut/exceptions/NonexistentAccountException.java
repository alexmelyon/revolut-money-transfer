package com.github.alexmelyon.revolut.exceptions;

public class NonexistentAccountException extends RevolutException {
    public NonexistentAccountException(int accountId) {
        super(RevolutError.NONEXISTENT_ACCOUNT, String.format("Account '%d' does not exists", accountId));
    }
}
